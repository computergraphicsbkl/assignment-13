#include <iostream>
#include "GL/glew.h"
#include "GL/freeglut.h"


GLuint program;
GLint coordAttribute, normalAttribute, colorAttribute;
GLint viewPositionUniform, lightPositionUniform, ambientUniform, diffuseUniform, specularUniform, shininessUniform;

GLuint vboVertex, vboNormal, vboColor, vboIndex;
GLint indicesCount;

float xRotation = 0;
float yRotation = 0;
float lightX = 0;
float lightY = 0;
float lightZ = -5;


// Вершинный шейдер.
const char* vertexShaderSource = R"(
attribute vec3 coord;
attribute vec3 normal;
attribute vec3 color;

uniform vec3 viewPosition;
uniform vec3 lightPosition;
uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;
uniform float shininess;

varying vec3 varColor;

void main()
{
    gl_Position = gl_ModelViewProjectionMatrix * vec4(coord, 1.0);

    vec3 position = gl_ModelViewMatrix * vec4(coord, 1.0);

    vec3 viewVector = viewPosition - position;
    vec3 normalVector = normalize(gl_NormalMatrix * normal);
    vec3 lightVector = normalize(lightPosition - position);

    float diffuseIntensity = max(dot(normalVector, lightVector), 0);
    float specularIntensity = max(pow(dot(reflect(-lightVector, normalVector), viewVector), shininess), 0);

    varColor = (ambient + diffuseIntensity * diffuse + specularIntensity * specular) * color;
}
)";


// Фрагментный шейдер.
const char* fragmentShaderSource = R"(
varying vec3 varColor;

void main()
{
    gl_FragColor = vec4(varColor, 1.0);
}
)";


void checkOpenGLError()
{
    GLenum errCode;
    if ((errCode = glGetError()) != GL_NO_ERROR)
        std::cout << "OpenGl error! - " << gluErrorString(errCode) << std::endl;
}


void initShaders()
{
    glewInit();

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    coordAttribute = glGetAttribLocation(program, "coord");
    normalAttribute = glGetAttribLocation(program, "normal");
    colorAttribute = glGetAttribLocation(program, "color");

    lightPositionUniform = glGetUniformLocation(program, "lightPosition");
    ambientUniform = glGetUniformLocation(program, "ambient");
    diffuseUniform = glGetUniformLocation(program, "diffuse");
    specularUniform = glGetUniformLocation(program, "specular");
    shininessUniform = glGetUniformLocation(program, "shininess");

    checkOpenGLError();
}


void initVBO()
{
    struct vertex
    {
        GLfloat x;
        GLfloat y;
        GLfloat z;
    };

    vertex vertices[] = {
            {-1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f, 1.0f, -1.0f},
            {-1.0f, 1.0f, -1.0f},
            {-1.0f, -1.0f, 1.0f},
            {1.0f, -1.0f, 1.0f},
            {1.0f, 1.0f, 1.0f},
            {-1.0f, 1.0f, 1.0f}
    };
    glGenBuffers(1, &vboVertex);
    glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    vertex normals[] = {
            {-1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f, 1.0f, -1.0f},
            {-1.0f, 1.0f, -1.0f},
            {-1.0f, -1.0f, 1.0f},
            {1.0f, -1.0f, 1.0f},
            {1.0f, 1.0f, 1.0f},
            {-1.0f, 1.0f, 1.0f}
    };
    glGenBuffers(1, &vboNormal);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);

    vertex colors[] = {
            {1.0f, 0.5f, 1.0f},
            {1.0f, 0.5f, 0.5f},
            {0.5f, 0.5f, 1.0f},
            {0.0f, 1.0f, 1.0f},
            {1.0f, 0.0f, 1.0f},
            {1.0f, 1.0f, 0.0f},
            {1.0f, 0.0f, 1.0f},
            {0.0f, 1.0f, 1.0f}
    };
    glGenBuffers(1, &vboColor);
    glBindBuffer(GL_ARRAY_BUFFER, vboColor);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    GLint indices[] = {
            0, 4, 5, 0, 5, 1,
            1, 5, 6, 1, 6, 2,
            2, 6, 7, 2, 7, 3,
            3, 7, 4, 3, 4, 0,
            4, 7, 6, 4, 6, 5,
            3, 0, 1, 3, 1, 2
    };
    indicesCount = sizeof(indices) / sizeof(indices[0]);
    glGenBuffers(1, &vboIndex);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndex);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    checkOpenGLError();
}


void render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Рисуем источник света.
    glPushMatrix();
    glTranslatef(lightX, lightY, lightZ);
    glutSolidSphere(0.1, 10, 10);
    glPopMatrix();

    // Готовим видовую матрицу для объекта.
    glPushMatrix();
    glTranslatef(0, 0, -10);
    glRotatef(xRotation, 1, 0, 0);
    glRotatef(yRotation, 0, 1, 0);

    glUseProgram(program);

    glUniform3fv(viewPositionUniform, 1, new float[3]{0, 0, 0});
    glUniform3fv(lightPositionUniform, 1, new float[3]{lightX, lightY, lightZ});
    glUniform3fv(ambientUniform, 1, new float[3]{0.1, 0.1, 0.1});
    glUniform3fv(diffuseUniform, 1, new float[3]{0.8, 0.8, 0.8});
    glUniform3fv(specularUniform, 1, new float[3]{0.1, 0.1, 0.1});
    glUniform1f(shininessUniform, 1);

    glEnableVertexAttribArray(coordAttribute);
    glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
    glVertexAttribPointer(coordAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(normalAttribute);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
    glVertexAttribPointer(normalAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(colorAttribute);
    glBindBuffer(GL_ARRAY_BUFFER, vboColor);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndex);
    glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(coordAttribute);
    glDisableVertexAttribArray(colorAttribute);

    glUseProgram(0);

    checkOpenGLError();

    glutSwapBuffers();
}


void reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float)width / height, 1, 1000);
}


void special(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_UP:
            xRotation += 5;
            break;
        case GLUT_KEY_DOWN:
            xRotation -= 5;
            break;
        case GLUT_KEY_LEFT:
            yRotation += 5;
            break;
        case GLUT_KEY_RIGHT:
            yRotation -= 5;
            break;
    }
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'w':
            lightY += 0.1;
            break;
        case 's':
            lightY -= 0.1;
            break;
        case 'd':
            lightX += 0.1;
            break;
        case 'a':
            lightX -= 0.1;
            break;
        case 'e':
            lightZ += 0.1;
            break;
        case 'q':
            lightZ -= 0.1;
            break;
    }
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(600, 600);
    glutCreateWindow("OpenGL");
    glutIdleFunc(render);
    glutDisplayFunc(render);
    glutReshapeFunc(reshape);
    glutSpecialFunc(special);
    glutKeyboardFunc(keyboard);

    glClearColor(0, 0, 0, 0);
    glEnable(GL_DEPTH_TEST);

    initShaders();
    initVBO();

    glutMainLoop();
}