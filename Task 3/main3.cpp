#include <iostream>
#include "GL/glew.h"
#include "GL/freeglut.h"
#include "SOIL/SOIL.h"


GLuint program;
GLint coordAttribute, normalAttribute, texCoordAttribute;
GLint viewPositionUniform, lightPositionUniform, ambientUniform, diffuseUniform, specularUniform, shininessUniform,
        textureSamplerUniform;

GLuint vboVertex, vboNormal, vboTextureCoord, vboIndex;
GLint indicesCount;

GLuint texture;
const char* textureFile = "fruits.bmp";

float xRotation = 0;
float yRotation = 0;
float lightX = 0;
float lightY = 0;
float lightZ = -5;


// Вершинный шейдер.
const char* vertexShaderSource = R"(
attribute vec3 coord;
attribute vec3 normal;
attribute vec2 texCoord;

uniform vec3 viewPosition;
uniform vec3 lightPosition;
uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;
uniform float shininess;

varying vec3 varIntensity;
varying vec2 varTexCoord;

void main()
{
    gl_Position = gl_ModelViewProjectionMatrix * vec4(coord, 1.0);

    vec3 position = gl_ModelViewMatrix * vec4(coord, 1.0);

    vec3 viewVector = viewPosition - position;
    vec3 normalVector = normalize(gl_NormalMatrix * normal);
    vec3 lightVector = normalize(lightPosition - position);

    float diffuseIntensity = max(dot(normalVector, lightVector), 0);
    float specularIntensity = max(pow(dot(reflect(-lightVector, normalVector), viewVector), shininess), 0);

    varIntensity = ambient + diffuseIntensity * diffuse + specularIntensity * specular;
    varTexCoord = texCoord;
}
)";


// Фрагментный шейдер.
const char* fragmentShaderSource = R"(
uniform sampler2D textureSampler;

varying vec3 varIntensity;
varying vec2 varTexCoord;

void main()
{
    gl_FragColor = vec4(varIntensity * texture(textureSampler, varTexCoord), 1.0);
}
)";


void checkOpenGLError()
{
    GLenum errCode;
    if ((errCode = glGetError()) != GL_NO_ERROR)
        std::cout << "OpenGl error! - " << gluErrorString(errCode) << std::endl;
}


void initShaders()
{
    glewInit();

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    coordAttribute = glGetAttribLocation(program, "coord");
    normalAttribute = glGetAttribLocation(program, "normal");
    texCoordAttribute = glGetAttribLocation(program, "texCoord");

    viewPositionUniform = glGetUniformLocation(program, "viewPosition");
    lightPositionUniform = glGetUniformLocation(program, "lightPosition");
    ambientUniform = glGetUniformLocation(program, "ambient");
    diffuseUniform = glGetUniformLocation(program, "diffuse");
    specularUniform = glGetUniformLocation(program, "specular");
    shininessUniform = glGetUniformLocation(program, "shininess");
    textureSamplerUniform = glGetUniformLocation(program, "textureSampler");

    checkOpenGLError();
}


void initVBO()
{
    struct vertex
    {
        GLfloat x;
        GLfloat y;
        GLfloat z;
    };

    struct uv
    {
        GLfloat u;
        GLfloat v;
    };

    vertex vertices[] = {
            {-1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f, 1.0f, -1.0f},
            {-1.0f, 1.0f, -1.0f},
            {-1.0f, -1.0f, 1.0f},
            {1.0f, -1.0f, 1.0f},
            {1.0f, 1.0f, 1.0f},
            {-1.0f, 1.0f, 1.0f}
    };
    glGenBuffers(1, &vboVertex);
    glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    vertex normals[] = {
            {-1.0f, -1.0f, -1.0f},
            {1.0f, -1.0f, -1.0f},
            {1.0f, 1.0f, -1.0f},
            {-1.0f, 1.0f, -1.0f},
            {-1.0f, -1.0f, 1.0f},
            {1.0f, -1.0f, 1.0f},
            {1.0f, 1.0f, 1.0f},
            {-1.0f, 1.0f, 1.0f}
    };
    glGenBuffers(1, &vboNormal);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);

    uv textureCoords[] = {
            {0, 0},
            {1, 0},
            {1, 1},
            {0, 1},
            {0, 0},
            {1, 0},
            {1, 1},
            {0, 1}
    };
    glGenBuffers(1, &vboTextureCoord);
    glBindBuffer(GL_ARRAY_BUFFER, vboTextureCoord);
    glBufferData(GL_ARRAY_BUFFER, sizeof(textureCoords), textureCoords, GL_STATIC_DRAW);

    GLint indices[] = {
            0, 4, 5, 0, 5, 1,
            1, 5, 6, 1, 6, 2,
            2, 6, 7, 2, 7, 3,
            3, 7, 4, 3, 4, 0,
            4, 7, 6, 4, 6, 5,
            3, 0, 1, 3, 1, 2
    };
    indicesCount = sizeof(indices) / sizeof(indices[0]);
    glGenBuffers(1, &vboIndex);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndex);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    checkOpenGLError();
}


void initTexture()
{
    texture = SOIL_load_OGL_texture("fruits.bmp", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
}


void render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Рисуем источник света.
    glPushMatrix();
    glTranslatef(lightX, lightY, lightZ);
    glutSolidSphere(0.1, 10, 10);
    glPopMatrix();

    // Готовим видовую матрицу для объекта.
    glPushMatrix();
    glTranslatef(0, 0, -10);
    glRotatef(xRotation, 1, 0, 0);
    glRotatef(yRotation, 0, 1, 0);

    glUseProgram(program);

    glUniform3fv(viewPositionUniform, 1, new float[3]{0, 0, 0});
    glUniform3fv(lightPositionUniform, 1, new float[3]{lightX, lightY, lightZ});
    glUniform3fv(ambientUniform, 1, new float[3]{0.1, 0.1, 0.1});
    glUniform3fv(diffuseUniform, 1, new float[3]{1.5, 1.5, 1.5});
    glUniform3fv(specularUniform, 1, new float[3]{0.5, 0.5, 0.5});
    glUniform1f(shininessUniform, 1);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(textureSamplerUniform, 0);

    glEnableVertexAttribArray(coordAttribute);
    glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
    glVertexAttribPointer(coordAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(normalAttribute);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
    glVertexAttribPointer(normalAttribute, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(texCoordAttribute);
    glBindBuffer(GL_ARRAY_BUFFER, vboTextureCoord);
    glVertexAttribPointer(texCoordAttribute, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndex);
    glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(coordAttribute);
    glDisableVertexAttribArray(texCoordAttribute);

    glUseProgram(0);

    checkOpenGLError();

    glutSwapBuffers();
}


void reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, (float)width / height, 1, 1000);
}


void special(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_UP:
            xRotation += 5;
            break;
        case GLUT_KEY_DOWN:
            xRotation -= 5;
            break;
        case GLUT_KEY_LEFT:
            yRotation += 5;
            break;
        case GLUT_KEY_RIGHT:
            yRotation -= 5;
            break;
    }
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'w':
            lightY += 0.1;
            break;
        case 's':
            lightY -= 0.1;
            break;
        case 'd':
            lightX += 0.1;
            break;
        case 'a':
            lightX -= 0.1;
            break;
        case 'e':
            lightZ += 0.1;
            break;
        case 'q':
            lightZ -= 0.1;
            break;
    }
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(600, 600);
    glutCreateWindow("OpenGL");
    glutIdleFunc(render);
    glutDisplayFunc(render);
    glutReshapeFunc(reshape);
    glutSpecialFunc(special);
    glutKeyboardFunc(keyboard);

    glClearColor(0, 0, 0, 0);
    glEnable(GL_DEPTH_TEST);

    initShaders();
    initVBO();
    initTexture();

    glutMainLoop();
}