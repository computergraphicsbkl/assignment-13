uniform mat4 rotationX;
uniform mat4 rotationY;
uniform mat4 rotationZ;
uniform mat4 scale;
uniform mat4 translate;
in vec3 coord;
in vec3 color;
varying vec3 var_color;

void main() {
	vec4 posx = rotationX * vec4(coord, 1.0);
	vec4 posy = rotationY * posx;
	vec4 posz = rotationZ * posy;
	gl_Position = translate * scale * posz;
	var_color = color;
}