﻿#define GLEW_STATIC

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <ctime>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

const char *vshaderPath = "vshader.glsl";
const char *fshaderPath = "fshader.glsl";
const char *rotfigurePath = "rotfigure.txt";
//! Переменные с индентификаторами ID
//! ID шейдерной программы
GLuint Program;
//! ID атрибута
GLint Attrib_vertex;
GLint Attrib_color;
GLuint VBO_vertex;
GLuint VBO_color;
GLuint VBO_element;
//вершины объекта
std::vector<glm::vec3> indexed_vertices;
//цвета дл¤ каждой вершины объекта
std::vector<glm::vec3> color_vertices;
//индексы вершин дл¤ соединения вершин в грани
std::vector<unsigned short> indices;
//! ID юниформ переменной цвета
GLint Unif_rotationX;
GLint Unif_rotationY;
GLint Unif_rotationZ;
GLint Unif_scale;
GLint Unif_translate;
float rotate_x = 0;
float rotate_y = 0;
float rotate_z = 0;
float scale_x = 1;
float scale_y = 1;
float scale_z = 1;
float translate_x = 0;
float translate_y = 0;
float translate_z = 0;

struct VertexData
{
	glm::vec3 vertex;
	bool operator<(const VertexData that) const
	{
		return memcmp((void*)this, (void*)& that, sizeof(VertexData)) > 0;
	};
};

//! Проверка ошибок OpenGL, если есть то вывод в консоль тип ошибки
void checkOpenGLerror()
{
	GLenum errCode;
	if ((errCode = glGetError()) != GL_NO_ERROR)
		std::cout << "OpenGl error! - " << gluErrorString(errCode);
}

//генерирует цвета для каждой вершины
void gen_colors(int size)
{
	color_vertices.clear();
	for (int i = 0; i < size; ++i)
	{
		glm::vec3 color;
		color.x = (float)rand() / RAND_MAX;
		color.y = (float)rand() / RAND_MAX;
		color.z = (float)rand() / RAND_MAX;
		color_vertices.push_back(color);
	}
}

char* readShader(const char* path) {
	ifstream fp;
	fp.open(path, ios_base::in);
	string buffer;
	if (fp) {
		string line;
		while (getline(fp, line)) {
			buffer.append(line);
			buffer.append("\r\n");
		}
	}
	else {
		cerr << "Error loading shader: " << path << endl;
	}
	char *result = new char[buffer.length() + 1];
	strcpy(result, buffer.c_str());
	return result;
}

void read_obj(const char * path, std::vector<glm::vec3>& out_vertices)
{
	std::vector<unsigned int> vertex_indices;
	std::vector<glm::vec3> temp_vertices;

	std::ifstream infile(path);
	std::string line;
	while (getline(infile, line))
	{
		std::stringstream ss(line);
		std::string firstSymbol;
		getline(ss, firstSymbol, ' ');
		if (firstSymbol == "v")
		{
			glm::vec3 vertex;
			ss >> vertex.x >> vertex.y >> vertex.z;
			temp_vertices.push_back(vertex);
		}
		else if (firstSymbol == "f")
		{
			unsigned int vertex_index[4];
			char slash;
			ss >> vertex_index[0] >> vertex_index[1] >> vertex_index[2] >> vertex_index[3];

			vertex_indices.push_back(vertex_index[0]);
			vertex_indices.push_back(vertex_index[1]);
			vertex_indices.push_back(vertex_index[2]);
			vertex_indices.push_back(vertex_index[3]);
		}
	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertex_indices.size(); ++i)
	{
		unsigned int vertexIndex = vertex_indices[i];
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		out_vertices.push_back(vertex);
	}
}

void indexVBO(std::vector<glm::vec3>& in_vertices, std::vector<unsigned short>& out_indices, std::vector<glm::vec3>& out_vertices);
void init() {
	std::vector<glm::vec3> vertices;
	read_obj("figure.obj", vertices);
	indexVBO(vertices, indices, indexed_vertices);
	gen_colors(indexed_vertices.size());
}


static void checkScaleBounds() {
	if (scale_x < 0.1f) scale_x = 0.1f;
	if (scale_y < 0.1f) scale_y = 0.1f;
	if (scale_z < 0.1f) scale_z = 0.1f;
}

//! Инициализация шейдеров
void initShader() {
	//! Исходный код шейдеров
	char* vsSource = readShader(vshaderPath);
	char* fsSource = readShader(fshaderPath);

	GLuint vShader, fShader;
	//! Создаем вершинный шейдер
	vShader = glCreateShader(GL_VERTEX_SHADER);
	//! Передаем исходный код
	glShaderSource(vShader, 1, &vsSource, NULL);
	//! Компилируем шейдер
	glCompileShader(vShader);

	//! Создаем фрагментный шейдер
	fShader = glCreateShader(GL_FRAGMENT_SHADER);
	//! Передаем исходный код
	glShaderSource(fShader, 1, &fsSource, NULL);
	//! Компилируем шейдер
	glCompileShader(fShader);

	//! Создаем программу и прикрепляем шейдеры к ней
	Program = glCreateProgram();
	glAttachShader(Program, vShader);
	glAttachShader(Program, fShader);
	//! Линкуем шейдерную программу
	glLinkProgram(Program);
	//! Проверяем статус сборки
	int link_ok;
	glGetProgramiv(Program, GL_LINK_STATUS, &link_ok);
	if (!link_ok)
	{
		std::cout << "error attach shaders \n";
		return;
	}
	//! Вытягиваем ID атрибута из собранной программы
	const char* attr_name = "coord";
	Attrib_vertex = glGetAttribLocation(Program, attr_name);
	if (Attrib_vertex == -1)
	{
		std::cout << "could not bind attrib " << attr_name << std::endl;
		return;
	}
	attr_name = "color";
	Attrib_color = glGetAttribLocation(Program, attr_name);
	if (Attrib_color == -1)
	{
		std::cout << "could not bind attrib " << attr_name << std::endl;
		return;
	}
	////! Вытягиваем ID юниформ
	char *unif_name = "rotationX";
	Unif_rotationX = glGetUniformLocation(Program, unif_name);
	if (Unif_rotationX == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "rotationY";
	Unif_rotationY = glGetUniformLocation(Program, unif_name);
	if (Unif_rotationY == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "rotationZ";
	Unif_rotationZ = glGetUniformLocation(Program, unif_name);
	if (Unif_rotationZ == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "scale";
	Unif_scale = glGetUniformLocation(Program, unif_name);
	if (Unif_scale == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "translate";
	Unif_translate = glGetUniformLocation(Program, unif_name);
	if (Unif_translate == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	checkOpenGLerror();
}

void indexVBO(std::vector<glm::vec3>& in_vertices, std::vector<unsigned short>& out_indices, std::vector<glm::vec3>& out_vertices)
{
	std::map<VertexData, unsigned short> VertexToOutIndex;

	for (unsigned int i = 0; i < in_vertices.size(); ++i)
	{
		VertexData packed = { in_vertices[i] };
		unsigned short index;
		auto it = VertexToOutIndex.find(packed);
		if (it != VertexToOutIndex.end()) // check if vertex already exists
			out_indices.push_back(it->second);
		else
		{
			out_vertices.push_back(in_vertices[i]);
			unsigned short newindex = (unsigned short)out_vertices.size() - 1;
			out_indices.push_back(newindex);
			VertexToOutIndex[packed] = newindex;
		}
	}
}

// ! Инициализация VBO
void initVBO() {
	glGenBuffers(1, &VBO_vertex);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_vertex);
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &VBO_color);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_color);
	glBufferData(GL_ARRAY_BUFFER, color_vertices.size() * sizeof(glm::vec3), &color_vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &VBO_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
}

// ! Освобождение VBO
void freeVBO()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &VBO_vertex);
	glDeleteBuffers(1, &VBO_color);
	glDeleteBuffers(1, &VBO_element);
}

//! Освобождение шейдеров
void freeShader()
{
	//! Передавая ноль, мы отключаем шейдрную программу
	glUseProgram(0);
	//! Удаляем шейдерную программу
	glDeleteProgram(Program);
}

void resizeWindow(int width, int height)
{
	glViewport(0, 0, width, height);
}

void setUniforms() {
	float angleX = 3.14f * rotate_x / 180.0f;
	float rotateX[] = {
		1, 0, 0, 0,
		0, cos(angleX), sin(angleX), 0,
		0, -sin(angleX), cos(angleX), 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_rotationX, 1, false, rotateX);

	float angleY = 3.14f * rotate_y / 180.0f;
	float rotateY[] = {
		cos(angleY), 0, -sin(angleY), 0,
		0, 1, 0, 0,
		sin(angleY), 0, cos(angleY), 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_rotationY, 1, false, rotateY);

	float angleZ = 3.14f * rotate_z / 180.0f;
	float rotateZ[] = {
		cos(angleZ), sin(angleZ), 0, 0,
		-sin(angleZ), cos(angleZ), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_rotationZ, 1, false, rotateZ);

	checkScaleBounds();
	float scale[] = {
		scale_x, 0, 0, 0,
		0, scale_y, 0, 0,
		0, 0, scale_z, 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_scale, 1, false, scale);

	float translate[] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		translate_x, translate_y, translate_z, 1 };
	glUniformMatrix4fv(Unif_translate, 1, false, translate);
}

//! Отрисовка
void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glEnable(GL_DEPTH_TEST);
	//! Устанавливаем шейдерную программу текущей
	glUseProgram(Program);

	setUniforms();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO_element);

	// ! Включаем массив атрибутов
	glEnableVertexAttribArray(Attrib_vertex);
	// ! Подключаем VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO_vertex);
	glVertexAttribPointer(Attrib_vertex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableClientState(GL_COLOR_ARRAY);
	glEnableVertexAttribArray(Attrib_color);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_color);
	glVertexAttribPointer(Attrib_color, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glColorPointer(3, GL_FLOAT, 0, 0);

	// ! Отключаем VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// ! Передаем данные на видеокарту (рисуем)
	glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, 0);
	// ! Отключаем массив атрибутов
	glDisableVertexAttribArray(Attrib_vertex);
	glDisableVertexAttribArray(Attrib_color);
	// ! Отключаем шейдерную программу
	glUseProgram(0);

	checkOpenGLerror();
	glutSwapBuffers();
}

void specialKeys(int key, int x, int y) {
	float angle;
	switch (key) {
	case GLUT_KEY_UP: rotate_x += 5; break;
	case GLUT_KEY_DOWN: rotate_x -= 5; break;
	case GLUT_KEY_RIGHT: rotate_y += 5; break;
	case GLUT_KEY_LEFT: rotate_y -= 5; break;
	case GLUT_KEY_PAGE_UP: rotate_z += 5; break;
	case GLUT_KEY_PAGE_DOWN: rotate_z -= 5; break;
	case GLUT_KEY_F1: scale_x += 0.1; break;
	case GLUT_KEY_F2: scale_y += 0.1; break;
	case GLUT_KEY_F3: scale_z += 0.1; break;
	case GLUT_KEY_F5: scale_x -= 0.1; break;
	case GLUT_KEY_F6: scale_y -= 0.1; break;
	case GLUT_KEY_F7: scale_z -= 0.1; break;
	case GLUT_KEY_F9: scale_x += 0.1; scale_y += 0.1; scale_z += 0.1; break;
	case GLUT_KEY_F11: scale_x -= 0.1; scale_y -= 0.1; scale_z -= 0.1; break;
	case GLUT_KEY_SHIFT_L: translate_x -= 0.1; break;
	case GLUT_KEY_CTRL_L: translate_y -= 0.1; break;
	case GLUT_KEY_ALT_L: translate_z -= 0.1; break;
	case GLUT_KEY_SHIFT_R: translate_x += 0.1; break;
	case GLUT_KEY_CTRL_R: translate_y += 0.1; break;
	case GLUT_KEY_ALT_R: translate_z += 0.1; break;
	}
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	setlocale(LC_ALL, "Rus");
	srand(time(0));
	init();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Lab 13");
	glClearColor(1, 1, 1, 1);
	//! Обязательно перед инициализацией шейдеров
	GLenum glew_status = glewInit();
	if (GLEW_OK != glew_status)
	{
		// ! GLEW не проинициализировалась
		std::cout << "Error: " << glewGetErrorString(glew_status) << "\n";
		return 1;
	}
	// ! Проверяем доступность OpenGL 2.0
	if (!GLEW_VERSION_2_0)
	{
		// ! OpenGl 2.0 оказалась не доступна
		std::cout << "No support for OpenGL 2.0 found\n";
		return 1;
	}
	// ! Инициализация
	initVBO();
	initShader();
	glutReshapeFunc(resizeWindow);
	glutDisplayFunc(render);
	glutSpecialFunc(specialKeys);
	glutMainLoop();
	// ! Освобождение ресурсов
	freeShader();
	freeVBO();
}